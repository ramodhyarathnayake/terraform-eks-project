provider "aws" {
#version = "~> 4.0"
  region  = "ap-southeast-1"
}

data "aws_availability_zones" "azs" {
  state = "available"
}

variable private_subnets_cidr_blocks {}
variable public_subnets_cidr_blocks {}
variable vpc_cidr_block {}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.14.2"
  
  name              = "TestApp-eks-vpc"
  cidr              = var.vpc_cidr_block
  azs               = data.aws_availability_zones.azs.names
  private_subnets   = var.private_subnets_cidr_blocks
  public_subnets    = var.public_subnets_cidr_blocks

  enable_nat_gateway    = true
  single_nat_gateway    = true
  enable_dns_hostnames  = true

  tags = {
    "kubernetes.io/cluster/TestApp-clsuter" = "shared"

  }

  public_subnet_tags = {
    "kubernetes.io/role/elb" = 1
    "kubernetes.io/cluster/TestApp-clsuter" = "shared"
 
}

  private_subnet_tags = {
      "kubernetes.io/role/internal-elb" = 1
      "kubernetes.io/cluster/TestApp-clsuter" = "shared"
 }

}