
module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "18.28.0"
  cluster_name = "TestApp-cluster"
  cluster_version = "1.23"
  subnet_ids = module.vpc.private_subnets
  vpc_id = module.vpc.vpc_id

  tags = {
    Environment = "dev"
    application = "TestApp"
  }
#https://aws-ia.github.io/terraform-aws-eks-blueprints/node-groups/

  eks_managed_node_groups = {
    ng-dev = {
      min_size     = 1
      max_size     = 5
      desired_size = 3

      instance_types = ["t2.micro"]
      capacity_type  = "SPOT"
    }
  }
}


# provider "kubernetes" {
#   host = data.aws_eks_cluster.TestApp-cluster.endpoint
#   token = data.aws_eks_cluster_auth.TestApp-cluster.token
#   cluster_ca_certificate = base64decode(data.aws_eks_cluster.TestApp-cluster.certificate_authority.0.data)
# }

# data "aws_eks_cluster" "TestApp-cluster"{
#   name = module.eks.cluster_id
# }

# data "aws_eks_cluster_auth" "TestApp-cluster" {
#   name = module.eks.cluster_id
# }


#  self_managed_node_groups = {
#         self_mg_wg_1 = {
#           node_group_name    = "self-managed_t2_small"
#           launch_template_os = "amazonlinux2eks"
#           subnet_ids         = module.vpc.private_subnets
#           instance_type      = "t2.small"
#           min_size           = 1
#           max_size           = 4
#         }
#         self_mg_wg_2 = {
#           node_group_name    = "self-managed_t2.micro"
#           launch_template_os = "amazonlinux2eks"
#           subnet_ids         = module.vpc.private_subnets
#           instance_type      = "t2.micro"
#           min_size           = 1
#           max_size           = 4
#         }
#    }
# }


